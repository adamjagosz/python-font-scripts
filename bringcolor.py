import os, sys, inspect, subprocess;
import re;
from xml.dom.minidom import parse, parseString
import codebase;
# codebase = "../!Codebase/FF_Python_Scripts/codebase.py";
# exec(open(codebase).read());

def _tags(elem, tag):
	return	elem.getElementsByTagName(tag);
def _tag(elem, tag):
	tags = _tags(elem, tag);
	if (len(tags) > 0):
		return tags[0];
	return	None;

def calcSubrBias(subrCount):
	subrBias = 0
	if (subrCount < 1240):
		subrBias = 107;
	elif (subrCount < 33900):
		subrBias = 1131;
	else:
		subrBias = 32768;
	return subrBias;

whiteSpaceRegex = re.compile(r"\s+", re.MULTILINE);
def cleanTextValue(node):
	textValue = node.firstChild.nodeValue;
	textValue = textValue.replace('  ',' ');
	textValue = whiteSpaceRegex.sub(" ", textValue).strip();
	return textValue;

class CharString:
	def __init__(self, charStringNode, ttx):
		self.ttx = ttx;
		self.node = charStringNode;
		self.glyphName = charStringNode.getAttribute('name');
		self.tokens = cleanTextValue(charStringNode).split(' ');
		self.calcTokensWithoutSubrBias();
		for mtx in _tags(ttx.hmtx, 'mtx'):
			if (mtx.getAttribute('name') == self.glyphName):
				self.width = mtx.getAttribute('width');
				self.lsb = mtx.getAttribute('lsb');
				break;

	def hasContours(self):
		return (self.tokens[0] != 'endchar' and
				self.tokens[1] != 'endchar');

	def calcTokensWithoutSubrBias(self):
		tokens = self.tokens[:];
		if ('callsubr' in tokens):
			index = 0;
			bias = self.ttx.subrBias;
			while index < len(tokens):
				if (tokens[index] == 'callsubr'):
					subrIndex = int(tokens[index-1]);
					subrIndex += bias;
					tokens[index-1] = str(subrIndex);
				index += 1;
		self.tokensWithoutSubrBias = tokens;
		
		## OK ##
		# if ('callsubr' in tokens):
			# print(self.tokens)
			# print(self.tokensWithoutSubrBias)

	def addSubrOffset(self, offset):
		tokens = self.tokensWithoutSubrBias[:];
		if ('callsubr' in tokens):
			index = 0;
			while index < len(tokens):
				if (tokens[index] == 'callsubr'):
					subrIndex = int(tokens[index-1]);
					subrIndex += offset;
					tokens[index-1] = str(subrIndex);
				index += 1;
		self.tokensWithoutSubrBias = tokens;
		
		## OK ##
		# if ('callsubr' in tokens):
			# print(self.tokensWithoutSubrBias)
			# print('+', offset)
			# print(self.tokens)

	def getTokensWithSubrBias(self, bias):
		tokens = self.tokensWithoutSubrBias[:];
		if ('callsubr' in tokens):
			index = 0;
			while index < len(tokens):
				if (tokens[index] == 'callsubr'):
					subrIndex = int(tokens[index-1]);
					subrIndex -= bias;
					tokens[index-1] = str(subrIndex);
				index += 1;
		## OK ##
		# if ('callsubr' in tokens):
			# print(self.tokensWithoutSubrBias)
			# print('-', bias)
			# print(tokens)
		return tokens;

class TTX:	
	def __init__(self, filename):
		self.dom = parse(filename);
		self.readTables();

	def processSubrs(self):
		if (not self.Subrs is None):
			self.SubrNodes = _tags(self.Subrs, 'CharString');
			self.SubrCount = self.SubrNodes.length;
			self.calcSubrBias();

	def processCharStrings(self):
		self.CharStringNodes = _tags(self.CharStrings, 'CharString');
		self.charStrings = [];
		for node in self.CharStringNodes:
			charString = CharString(node, self);
			self.charStrings.append(charString);

	def calcSubrBias(self):
		self.subrBias = calcSubrBias(self.SubrCount);

	def readTables(self):
		self.Subrs = _tag(self.dom,'Subrs');
		self.processSubrs();
		self.GlyphOrder = _tag(self.dom,'GlyphOrder');
		self.GlyphClassDef = _tag(self.dom,'GlyphClassDef');
		self.hmtx = _tag(self.dom,'hmtx');
		self.lastGlyphID = int(_tags(self.GlyphOrder,'GlyphID')[-1]\
								.getAttribute('id'));
		self.CharStrings = _tag(self.dom, 'CharStrings');
		self.processCharStrings();
		self.name = _tag(self.dom, 'name');

	def applyCharStrings(self):
		CharStringsClone = self.CharStrings.cloneNode(False); #removes children
		for charString in self.charStrings:
			nodeClone = charString.node.cloneNode(False);
			nodeClone.setAttribute('name', charString.glyphName);
			tokens = charString.getTokensWithSubrBias(self.subrBias);
			nodeClone.appendChild(self.dom.createTextNode(' '.join(tokens)));
			CharStringsClone.appendChild(nodeClone);
		old = _tag(self.dom, 'CharStrings')
		old.parentNode.replaceChild(CharStringsClone, old);
		self.CharStrings = _tag(self.dom, 'CharStrings');

	def appendSubrs(self, layerTtx):
		subrOffset = self.SubrCount;
		for subr in layerTtx.SubrNodes:
			subrClone = subr.cloneNode(True);
			### the 'index' attribute of 'CharString' element 
			### is supposedly only human-readable
			index = int(subr.getAttribute('index'));
			subrClone.setAttribute('index',str(index+subrOffset));
			self.Subrs.appendChild(subrClone);

	def updateColorEntry(self, targetGlyphName, layerGlyphName):
		ColorGlyphs = _tags(self.COLR, 'ColorGlyph');
		targetNode = None;
		for ColorGlyph in ColorGlyphs:
			if (ColorGlyph.getAttribute('name') == targetGlyphName):
				targetNode = ColorGlyph;
		if (targetNode == None):
			targetNode = parseString('<ColorGlyph name="' + targetGlyphName +'">\
				</ColorGlyph>').documentElement;
			# <layer colorID="' + str(self.color0) + \
			# '" name="' + targetGlyphName + '"/>\
			self.COLR.appendChild(targetNode);
		layerNode = parseString('<layer colorID="' +\
				str(self.colorIndices[self.currentColorIndex]) +
				'" name="' + layerGlyphName + '"/>')\
				.documentElement;
		targetNode.appendChild(layerNode);

	def appendColorLayer(self, layerTtx):
		self.appendSubrs(layerTtx);
		subrOffset = self.SubrCount;
		self.processSubrs();

		for charString in layerTtx.charStrings:
			if (not charString.hasContours()):
				continue;

			self.lastGlyphID += 1;
			idStr = str(self.lastGlyphID);
			layerGlyphName = 'glyph' + idStr;

			# <GlyphOrder>
			GlyphID = self.dom.createElement('GlyphID');
			GlyphID.setAttribute('id', idStr);
			GlyphID.setAttribute('name', layerGlyphName);
			self.GlyphOrder.appendChild(GlyphID);
			
			# <GDEF><GlyphClassDef>
			if (self.GlyphClassDef != None):
				ClassDef = self.dom.createElement('ClassDef');
				ClassDef.setAttribute('glyph', layerGlyphName);
				ClassDef.setAttribute('class', '1');
				self.GlyphClassDef.appendChild(ClassDef);
			
			# <hmtx>
			mtx = self.dom.createElement('mtx');
			mtx.setAttribute('name', layerGlyphName);
			mtx.setAttribute('width', charString.width);
			mtx.setAttribute('lsb', charString.lsb);
			self.hmtx.appendChild(mtx);

			targetGlyph = charString.glyphName;
			self.updateColorEntry(targetGlyph, layerGlyphName);
			
			charString.addSubrOffset(subrOffset);
			charString.glyphName = layerGlyphName;
			self.charStrings.append(charString);
		print(len(_tags(self.CharStrings, 'CharString')));
		print('self.applyCharStrings()');
		self.applyCharStrings();
		print(len(_tags(self.CharStrings, 'CharString')));
		print('self.processCharStrings()');
		self.processCharStrings();
		print(len(_tags(self.CharStrings, 'CharString')));
		self.currentColorIndex += 1;
	
	def appendSvgLayersToTtx(self, svgFilenames, colors):
		SVG = self.dom.createElement('SVG');
		svgDoc = parseString('<svgDoc endGlyphID="196" startGlyphID="34"></svgDoc>').documentElement;
		
		svg = parseString('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox=" 0 0 1000 1000"></svg>');
		
		cdata = self.dom.createCDATASection(svg.toxml());

		
		
		svgDoc.appendChild(cdata);
		SVG.appendChild(svgDoc);
		_tag(self.dom, 'ttFont').appendChild(SVG);
		
		svgLayers = [];
		for svgFile in svgFilenames:
			svgDom = parse(svgFile);
			glyphs = _tags(svgDom, 'glyph');
			svgLayers.append(glyphs);
		
		# for ColorGlyph in _tags(self.COLR, 'ColorGlyph'):
			# name = ColorGlyph.getAttribute('name');
			# for layer in svgLayers:
				# for glyph in layer:
					# if (glyph.getAttribute('glyph-name') == name):
						# svgColorGlyph = self.dom.createElement(')
	
	def updateNames(self, basename, style, weight):
		names = codebase.NameTable(basename, style, weight);
		for namerecord in _tags(self.name, 'namerecord'):
			id = int(namerecord.getAttribute('nameID'));
			if (id == 1):
				namerecord.firstChild.nodeValue = names.family;
				continue;
			if (id == 2):
				namerecord.firstChild.nodeValue = names.subFamily;
				continue;
			if (id == 4):
				namerecord.firstChild.nodeValue = names.spaceName;
				continue;
			if (id == 6):
				namerecord.firstChild.nodeValue = names.hyphenName;
				continue;
			if (id == 16):
				namerecord.firstChild.nodeValue = names.preferredFamily;
				continue;
			if (id == 17):
				namerecord.firstChild.nodeValue = names.preferredSubFamily;
				continue;
					
		
	
	def initColor(self, colors):
		paletteColors = colors[:];
		self.colors = colors[:];
		self.colorIndices = [];		
		self.currentColorIndex = 0;

		colorCount = len(paletteColors);
		colorIndex = 0;
		while colorIndex < len(paletteColors):
			if (paletteColors[colorIndex] == ""):
				self.colorIndices.append(0xFFFF);
				paletteColors.pop(colorIndex);
			else:
				self.colorIndices.append(colorIndex);
				colorIndex += 1;
		print(colors);
		print(self.colorIndices);
		
		self.CPAL = parseString('<CPAL>\
				<version value="0"/>\
				<numPaletteEntries value="'+str(len(paletteColors))+'"/>\
				<palette index="0">\
				</palette>\
			</CPAL>').documentElement;
		palette = _tag(self.CPAL, 'palette');
		
		colorIndex = 0;
		for color in paletteColors:
			colorNode = parseString(
					'<color index="'+str(colorIndex)+'" value="#'+color+'"/>').documentElement;
			colorIndex += 1;
			palette.appendChild(colorNode);
		
		GDEF = _tag(self.dom, 'GDEF');
		GDEF.parentNode.insertBefore(self.CPAL, GDEF);
		
		self.COLR = parseString('<COLR><version value="0"/></COLR>').documentElement;
		GDEF.parentNode.insertBefore(self.COLR, GDEF);
	
	def modifyCPAL(self, colors):
		colorTags = _tags(self.CPAL, 'color');
		i = 0;
		for colorTag in colorTags:
			colorTag.setAttribute('value', '#' + colors[i]);
			i += 1;
		

def mergeTtxLayers(dir, monoChromeFilename, layerFilenames, colors, basename, style, weight):
	baseTtx = TTX(monoChromeFilename);
	baseTtx.initColor(colors);
	baseTtx.updateNames(basename, style, weight);
	
	colorLayerTtx = [];
	for filename in layerFilenames:
		colorLayerTtx.append(TTX(filename));

	for ttx in colorLayerTtx:
		baseTtx.appendColorLayer(ttx);
	return baseTtx;

# styles[x] + colorLayers[x] is supposed to be full style name if you know what I mean. E.g.: Arial Rounded Red Italic:
# basename = Arial, style = Rounded, color = Red, Weight = Italic
# but in the name table:
#  id 1 = Arial Rounded Red
#  id 2 = Italic
#  id 16 = Arial
#  id 17 = Rounded Red Italic
def generateTTX(dir, basename, style, weight, colorLayer, format = 'otf'):
	hyphenName = codebase.getHyphenName([
		basename, style, colorLayer, weight]);
	sourceFile = dir + "\Output\\" + hyphenName + "." + format;
	targetDir = dir + "\Temp\\";
	command = "ttx -f -d " + targetDir + " " + sourceFile;
	subprocess.run(command, shell=True, check=True);
		
def generateColorTTX(dir, basename, style, weight, colorLayers, colors, targetBasename, targetStyle, monochromeLayer = None):
	if (monochromeLayer == None):
		monochromeLayer = colorLayers[0];
	hyphenName = codebase.getHyphenName([
			basename, style, monochromeLayer, weight]);
	monoChromeFilename = dir + "\Temp\\" + hyphenName + ".ttx";
	layerFilenames = [];
	svgFilenames = [];
	for color in colorLayers:
		hyphenName = codebase.getHyphenName([
			basename, style, color, weight]);
		sourceFile = dir + "\Temp\\" + hyphenName + ".ttx";
		svgSourceFile = dir + "\Temp\\" + hyphenName + ".svg";
		layerFilenames.append(sourceFile);
		svgFilenames.append(svgSourceFile);
	
	print('Generating Color TTX for ' + monoChromeFilename)
	colorTtx = mergeTtxLayers(dir, monoChromeFilename, layerFilenames, colors, targetBasename, targetStyle, weight);
	
	# colorTtx.appendSvgLayersToTtx(svgFilenames, colors);
	
	hyphenName = codebase.getHyphenName([targetBasename, targetStyle, weight]);
	targetFilename = dir + "\Temp\\" + hyphenName + ".ttx";
	targetFile = open(targetFilename, 'w');
	targetFile.write(colorTtx.dom.toprettyxml());
	return colorTtx;
	
def modifyColorTTX(colorTtx, colors, dir, basename, style, weight):
	colorTtx.modifyCPAL(colors);
	colorTtx.updateNames(basename, style, weight);
	
	hyphenName = codebase.getHyphenName([basename, style, weight]);
	targetFilename = dir + "\Temp\\" + hyphenName + ".ttx";
	targetFile = open(targetFilename, 'w');
	targetFile.write(colorTtx.dom.toprettyxml());

def generateColorFont(dir, basename, style, weight):
	hyphenName = codebase.getHyphenName([basename, style, weight]);
	sourceFile = dir + "\Temp\\" + hyphenName + ".ttx";
	targetDir = dir + "\Output\\";
	command = "ttx -f -d " + targetDir + " " + sourceFile;
	subprocess.run(command, shell=True, check=True);
				