# -*- coding: utf-8 -*-

def optional_join(array, delimiter = ' ', undesired = '?') :
	str = delimiter.join(map(lambda x: x.replace(undesired,delimiter), array))\
		.replace(delimiter + delimiter, delimiter).strip(delimiter)\
		.replace(delimiter + delimiter, delimiter).strip(delimiter)\
		.replace(delimiter + delimiter, delimiter).strip(delimiter);
	return str;

def getHyphenName(array):
	familySpaceless = optional_join(array[:-1], "", " ");
	hyphenName = optional_join([familySpaceless, array[-1]], "-", " ");
	return hyphenName;

class NameTable:
	def __init__(self, basename, style, weight):
		# if (weight not in ['Regular', 'Italic', 'Oblique', 'Bold', 'Bold Italic', 'Bold Oblique']):
		if (weight not in ['Regular', 'Bold']):
			style = style + " " + weight;
			weight = 'Regular';
		self.weight = weight.replace(' Italic','');
		# self.weight = self.weight.replace(' Oblique','');
		self.weight = self.weight.replace('Italic','');
		# self.weight = self.weight.replace('Oblique','');
		if self.weight == '':
			self.weight = 'Regular';
		self.family = optional_join([basename, style], " ", "?");
		self.subFamily = weight;
		self.preferredFamily = basename;
		self.preferredSubFamily = optional_join([style, weight], " ", "?");
		self.spaceName = optional_join([basename, style, weight], " ", "?");
		self.hyphenName = getHyphenName([basename, style, weight]);

def generateFont(dir, font, basename, style, weight, format = 'otf', description = ''):

	print ('\n\n\n#### Using deprecated function ####\n\n\n');
	names = NameTable(basename, style, weight);
	font.appendSFNTName('English (US)', 0, '(c) 2018 Adam Jagosz. All Rights Reserved.');
	font.appendSFNTName('English (US)', 'Family', names.family);
	font.appendSFNTName('English (US)', 'SubFamily', names.subFamily);
	font.appendSFNTName('English (US)', 3, 'Adam Jagosz: ' + names.spaceName); #UniqueID
	font.appendSFNTName('English (US)', 'Fullname', names.spaceName);
	font.appendSFNTName('English (US)', 'PostScriptName', names.hyphenName);
	if (names.family != names.preferredFamily):
		font.appendSFNTName('English (US)', 'Preferred Family',
						names.preferredFamily);
	if (names.subFamily != names.preferredSubFamily):
		font.appendSFNTName('English (US)', 'Preferred Styles',
						names.preferredSubFamily);
	font.appendSFNTName('English (US)', 7, basename + ' is a trademark of Adam Jagosz.');
	font.appendSFNTName('English (US)', 8, 'Adam Jagosz'); #manufacturer
	font.appendSFNTName('English (US)', 9, 'Adam Jagosz'); #designer
	if (description != ''):
		font.appendSFNTName('English (US)', 10, description);
	font.appendSFNTName('English (US)', 11, 'http://adamjagosz.com');
	font.appendSFNTName('English (US)', 12, 'http://adamjagosz.com');
	# font.appendSFNTName('English (US)', 13, license);
	# font.appendSFNTName('English (US)', 14, 'http://adamjagosz.com/fonts/license');
	
	font.weight = names.weight;
	font.os2_vendor = 'Adam';
	
	filename = names.hyphenName + "." + format;
	
	font.generate(dir + "\\" + filename, 
			flags = ('opentype','old-kern', 'dummy-dsig'));
	print ("Font '" + filename + "' generated.");
	return;
	
	
	
class NameTable2:
	def __init__(self, basename, variant, weight, style):
		# if (weight not in ['Regular', 'Italic', 'Oblique', 'Bold', 'Bold Italic', 'Bold Oblique']):
		preferredFamily = optional_join([basename, variant]);
		
		RIBIfamily = preferredFamily;
		RBweight = weight;
		RIstyle = style;
		if (weight not in ['Regular', 'Bold']):
			RBweight = 'Regular';
			RIBIfamily = optional_join([RIBIfamily, weight]);
		if (style not in ['', 'Italic', 'Oblique']):
			RIstyle = '';
			RIBIfamily = optional_join([RIBIfamily, style]);
		if (RBweight == 'Regular' and RIstyle != ''):
			RIBIsubFamily = RIstyle;
			preferredSubFamily = style;
		else:
			RIBIsubFamily = optional_join([RBweight, RIstyle]);
			preferredSubFamily = optional_join([weight, style]);
		
		weights = {'Regular': 400, 'Bold': 700, 'Black': 900};
		self.weight = weight;
		self.os2_weight = weights[weight];
		self.family = RIBIfamily;
		self.subFamily = RIBIsubFamily;
		self.preferredFamily = preferredFamily;
		self.preferredSubFamily = preferredSubFamily;
		self.spaceName = preferredFamily + ' ' + preferredSubFamily;
		self.simpleName = preferredFamily + ((' ' + preferredSubFamily) if preferredSubFamily != 'Regular' else '');
		self.hyphenName = (preferredFamily + '-' + preferredSubFamily).replace(' ','');
		
		# print('---')
		# print('RIBIfamily', RIBIfamily);
		# print('RIBIsubFamily', RIBIsubFamily)
		
		# print('preferredFamily', preferredFamily);
		# print('preferredSubFamily', preferredSubFamily)
		# print('spaceName', self.spaceName);
		# print('hyphenName', self.hyphenName)

def generateFont2(dir, font, basename, weight, style, format = 'otf', description = ''):
	names = NameTable2(basename, '', weight, style);
	
	font.appendSFNTName('English (US)', 0, '© 2018 Adam Jagosz. All Rights Reserved.');
	font.appendSFNTName('English (US)', 'Family', names.family);
	font.familyname = names.family;
	font.appendSFNTName('English (US)', 'SubFamily', names.subFamily);
	font.appendSFNTName('English (US)', 3, 'Adam Jagosz: ' + names.spaceName); #UniqueID
	# font.appendSFNTName('English (US)', 'Fullname', names.spaceName);
	font.appendSFNTName('English (US)', 'Fullname', names.simpleName);
	font.fullname = names.simpleName;
	font.appendSFNTName('English (US)', 'PostScriptName', names.hyphenName);
	font.fontname = names.hyphenName;
	if (names.family != names.preferredFamily):
		font.appendSFNTName('English (US)', 'Preferred Family',
						names.preferredFamily);
	if (names.subFamily != names.preferredSubFamily):
		font.appendSFNTName('English (US)', 'Preferred Styles',
						names.preferredSubFamily);
	font.appendSFNTName('English (US)', 7, basename + ' is a trademark of Adam Jagosz.');
	font.appendSFNTName('English (US)', 8, 'Adam Jagosz'); #manufacturer
	font.appendSFNTName('English (US)', 9, 'Adam Jagosz'); #designer
	if (description != ''):
		font.appendSFNTName('English (US)', 10, description);
	font.appendSFNTName('English (US)', 11, 'http://adamjagosz.com');
	font.appendSFNTName('English (US)', 12, 'http://adamjagosz.com');
	# font.appendSFNTName('English (US)', 13, license);
	# font.appendSFNTName('English (US)', 14, 'http://adamjagosz.com/fonts/license');
	
	font.os2_weight = names.os2_weight;
	font.weight = names.weight;
	font.os2_vendor = 'Adam';
	
	
	fsTypes = ['Regular', 'Italic', 'Bold', 'Bold Italic']
	bold = fsTypes.index(names.subFamily) > 1;
	italic = fsTypes.index(names.subFamily) % 2 == 1;
	
	
	filename = names.hyphenName + "." + format;
	font.generate(dir + "\\" + filename, 
			flags = ('opentype','old-kern', 'dummy-dsig'));
	print ("\n   Font '" + filename + "' generated.\n");
	return;
