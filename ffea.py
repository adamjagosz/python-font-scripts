#!python3
## process .ffea (my shorthand language for fea, similar to what LESS, SASS are for CSS) to produce .fea (OpenType feature file) compatible with FontForge (annexing the features with language tags).

import sys;

sourceFile = sys.argv[1];
targetFile = sourceFile[:-4] + 'fea';

languagesystems = [];
features = {};
currentFeatureTag = '';
commonLookups = [];
languageSpecificLookups = {};
languageExcludeDflt = {};
currentFeature = [];
script, language = ('','');

target = open(targetFile, "w")
with open(sourceFile, "r") as src:
	data = src.readlines();
 
	inFeature = False;
	inLookup = False;
	inSpecificLookups = False;
	inIgnoredComment = False;
	
	for line in data:
		if (line.strip(' ').startswith('languagesystem ')):
			languagesystems.append(tuple(line.replace(';','').strip().split()[1:3]));
	
	for line in data:
		line = line.strip(' ');
		if (line.startswith('#*')):
			inIgnoredComment = not inIgnoredComment;
		elif (inIgnoredComment):
			continue;
		elif (line.strip().startswith('feature ')):
			inFeature = True;
			currentFeatureTag = line.split()[1];
			currentFeature = [];
			commonLookups = [];
			languageSpecificLookups = {};
			languageExcludeDflt = {};
			target.write(line);
		elif (inFeature and line.strip().startswith('lookup ') and line.strip().endswith('{')):
			inLookup = True;
			commonLookups.append(line);
		elif (inLookup and line.strip().startswith('}')):
			inLookup = False;
			commonLookups.append(line);
		elif (inFeature and line.strip().startswith('}')):
			inFeature = False;
			inSpecificLookups = False;
			lastScript = '';
			anyLanguageSpecificLookups = False;
			for languagesystem in languagesystems:
				if(languagesystem in languageSpecificLookups):
					anyLanguageSpecificLookups = True;
			if (not anyLanguageSpecificLookups):
				currentFeature.extend(commonLookups);
			else:
				for languagesystem in languagesystems:
					script = languagesystem[0];
					language = languagesystem[1];
					languageSpecificLookupsPresent = languagesystem in languageSpecificLookups;
					if (len(commonLookups) > 0 or languageSpecificLookupsPresent):
						if (script != lastScript):
							lastScript = script;
							currentFeature.append('script ' + script + ' ;\n');
						currentFeature.append('  language ' + language + ' exclude_dflt;\n');
					if (not languageSpecificLookupsPresent or not languageExcludeDflt[(script,language)]):
						currentFeature.extend(commonLookups);
					if (languageSpecificLookupsPresent):
						currentFeature.extend(languageSpecificLookups[languagesystem]);
			features[currentFeatureTag] = currentFeature;
			target.writelines(currentFeature);
			target.write(line);
		elif (inFeature and line.startswith('script')):
			inSpecificLookups = True;
			script = line.replace(';',' ;').split()[1];
		elif (inFeature and line.startswith('language')):
			tokens = line.replace(';',' ;').split();
			language = tokens[1];
			languageExcludeDflt[(script,language)] = tokens[2] == 'exclude_dflt';
			languageSpecificLookups[(script,language)] = [];
		elif (inSpecificLookups):
			languageSpecificLookups[(script,language)].append(line);
		elif (inFeature):
			commonLookups.append(line);
		elif (line.startswith('#! feature ')):
			targetFeatureTag = line.split()[2];
			sourceFeatureTag = line.split()[4];
			target.write('feature ' + targetFeatureTag + ' {\n');
			target.writelines(features[sourceFeatureTag]);
			target.write('} ' + targetFeatureTag + ';\n');
		else:
			target.write(line);

target.close();