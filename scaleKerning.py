## scale kerning by factor 1.295

target = open("E:\Adam\OneDrive\Fonts\Rywalka\Rywalka Regular Narrow_kerning_scaled.fea", "w")


def scaleIfInt(s):
	try:
		return str(int(float(s) * 1.295))
	except ValueError:
		return s



with open("E:\Adam\OneDrive\Fonts\Rywalka\Rywalka Regular Narrow_kerning.fea", "r") as src:
	data = src.readlines()
 
	for line in data:
		line = line.replace(';',' ;');
		words = line.split()
		words = list(map(lambda word: scaleIfInt(word), words))
		target.write(' '.join(words) + '\n')

target.close();