import os
import sys
import importlib
import fontforge
import inspect

script_dir = os.path.dirname(os.path.abspath(inspect.stack()[0][1]))

mkmkPrefix = 'mk-';

# Copies anchors from referenced glyphs to the target glyph. If any of the references is a mark and has a mark anchor at y=bottomMarkHeight (e.g. 0) then the anchors from the referenced base glyph at y=bottomMarkHeight are not copied. If any of the references is a mark with a mark anchor at height other than bottomMarkHeight, it is deemed a top accent and top anchors from the base glyph are not copied. In place of the omitted base glyph anchors, the basemark anchors from the referenced marks are copied as base anchors, taking into consideration the reference's translation. Expected class name of the basemark anchor is 'mk-xxx', then the copied anchor will receive class 'xxx', where 'xxx' may be e.g. 'above', 'below', etc.

def copyAnchors(target, bottomMarkHeight):
	baseGlyphTuple = None;
	markGlyphs = [];
	for ref in target.references:
		font.selection.select(ref[0]);
		refX, refY = ref[1][4:6];
		isBaseTransformed = ref[1][0:4] != (1.0, 0, 0, 1.0);
		for refGlyph in font.selection.byGlyphs: #iterator has 1 element
			for anchor in refGlyph.anchorPoints:
				if (anchor[1] == 'base'):
					baseGlyphTuple = (refGlyph, refX, refY);
					break;
				elif (anchor[1] == 'mark'):
					markGlyphs.append((refGlyph, refX, refY));
					break;
	if(baseGlyphTuple != None):
		hasMarkAbove = False;
		hasMarkBelow = False;
		for markGlyphTuple in markGlyphs:
			markGlyph = markGlyphTuple[0];
			for markAnchor in markGlyph.anchorPoints:
				if (markAnchor[1] == 'mark'):
					if(markAnchor[3] == bottomMarkHeight):
						hasMarkBelow = True;
					elif(markAnchor[3] > bottomMarkHeight):
						hasMarkAbove = True;
				elif (markAnchor[1] == 'basemark'):
					target.addAnchorPoint(markAnchor[0][len(mkmkPrefix):], 'base', markAnchor[2] + markGlyphTuple[1], markAnchor[3] + markGlyphTuple[2]);
		baseGlyph = baseGlyphTuple[0];
		if(isBaseTransformed):
			print ('Omitting glyph ' + target.glyphname + ', base is rotated or flipped');
		elif ('preserve-anchors' in target.comment):
			print ('Omitting glyph ' + target.glyphname + ', glyph comment: ' + target.comment);
		else:
			for anchor in baseGlyph.anchorPoints:
				if(not hasMarkAbove and anchor[3] > bottomMarkHeight or not hasMarkBelow and anchor[3] <= bottomMarkHeight):
					target.addAnchorPoint(anchor[0], anchor[1], anchor[2] + baseGlyphTuple[1], anchor[3] + baseGlyphTuple[2]);

font_path = sys.argv[1];
bottomMarkHeight = 0;
font = fontforge.open(font_path);
for glyph in font.glyphs():
	copyAnchors(glyph, bottomMarkHeight);
font.save(font_path[:-4] + '_anchors.sfd');

# fontforge.registerMenuItem(buildAccentedGlyphs,None,None,("Font","Glyph"),None,"Glyphbuilder");
