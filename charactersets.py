
import sys, re, unicodedata;

class SFD:
	def __init__(self, filename):
		glyphs = {};

		with open(filename, "r") as src:
			data = src.readlines();
			
			glyphName = '';
			encoding = 0;
			
			for line in data:
				
				# StartChar: space
				# Encoding: 1 32 1
				startCharToken = 'StartChar: ';
				encodingToken = 'Encoding: ';
				if (line.startswith(startCharToken)):
					glyphName = re.sub(
						r'\s', '',
						line[len(startCharToken):]
						);
				elif (glyphName != '' and line.startswith(encodingToken)):
					encoding = int(line[len(encodingToken):].split(' ')[1]);
					glyphs[glyphName] = encoding;
		self.glyphs = glyphs;
		self.uc = [];
		self.lc = [];
				
		
		specialCompositions = { 'IJacute': 'ÍJ́', 'Jacute': 'J́', 'ijacute': 'íj́', 'jacute': 'j́' };
			
		print ('');
	
		for glyphName, encoding in self.glyphs.items():
			characters = '';
			if (encoding > -1):
				characters = chr(encoding);
			else:
				i = glyphName.find('.');
				baseName = glyphName;
				if (i >= 0):
					baseName = glyphName[:i];
				if (baseName in specialCompositions):
					characters = specialCompositions[baseName];
				else:
					parts = baseName.split('_');
					for part in parts:
						if (part in self.glyphs and glyphs[part] > -1):
							encoding = glyphs[part];
							characters += chr(encoding);
						else:
							characters = '';
							print ('[!] unclassified: ' + glyphName);
							break;

			if (characters.isupper()):
				self.uc.append(glyphName);
			if (characters.islower()):
				self.lc.append(glyphName);
				# print (glyphName + ' (' + unicodedata.normalize('NFD', character) + '): ' + str(encoding));
		print ('');
				
	def printUc(self):
		print ('@uc = [' + ' '.join(self.uc) + '] ;\n');
		
	def printLc(self):
		print ('@lc = [' + ' '.join(self.lc) + '] ;\n');
		
	def printDependent(self, baseCharacter):
		dependent = [];
		for glyphName, encoding in self.glyphs.items():
			if (encoding == -1):
				continue;
			character = chr(encoding);
			components = unicodedata.normalize('NFD', character);
			if (components[0] == baseCharacter):
				dependent.append(glyphName);
		print ('@' + baseCharacter +'_accents = [ ' + ' '.join(dependent) + ' ] ;');